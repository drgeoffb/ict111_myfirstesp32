// define the LED pin
LED1 = D16;

// a variable to hold the LED state (true/false = on/off)
var  led_on = false;

// WiFi configuration
var WIFI_NAME = "wifi_name_here";
var WIFI_PASSWORD = "wifi_password_here";
var WIFI_OPTIONS = {
	password: WIFI_PASSWORD
};

// make a wifi object by loading the wifi module
var wifi = require("Wifi");

// connect to wifi
wifi.connect(WIFI_NAME, WIFI_OPTIONS, function(error) {
	if (error) {
		console.log("Wifi connection error: " + error);
		return;
	}

	// if we reach here, we're connected to wifi. 
	console.log("Wifi connected!");
	console.log("  IP Address: " + wifi.getIP().ip);
});

// SPI bus configuration
var CHIP_SELECT = D5;
var CLOCK = D18;
var MOSI = D23;
var MISO = D19;

// make an SPI object by loading the built-in SPI module
var spi = new SPI();

// configure the spi object
spi.setup({
	sck: CLOCK,
	miso: MISO,
	mosi: MOSI
});

// create a near field communications (NFC) object using the library 
// for the MFRC522 chip and connect it to the card reader
var nfc = require("MFRC522").connect(spi, CHIP_SELECT);

// a function to convert decimal numbers to hexadecimal
function toHex(d) {
    return  ("0"+(Number(d).toString(16))).slice(-2).toUpperCase();
}

// set an interval to scan for cards 
setInterval(function() {
	nfc.findCards(function(card) {
		// if we found a card, get it's UID
		var card_uid = "";

		// nfc.findCards() gives us the card uid as an array of bytes
		// to assemble them into a string for printing, loop 
		// through them one by one...
		for (var i = 0; i < card.length; i++) {
			// convert each byte to a 2 digit hexadecimal string
			// and add it to the end of the card_uid string
			card_uid = card_uid + toHex(card[i]);

			// if this is not the last byte, at a ':' character to 
			// the end of the string
			if (i < card.length - 1 ) {
				card_uid = card_uid + ":";
			}
		}
		// print the card's UID to the console
		console.log("Found card with UID: " + card_uid);
	});
}, 1000);

// an interval (a function that repeats at regular time intervals)
setInterval(function() {
	// flip the LED state from true to false, or false to true
  	led_on = !led_on;

  	// write the LED state to the pin where the LED is attached
  	LED1.write(led_on);

}, 500);
